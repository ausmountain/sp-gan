mu100;
nu100;
eps100;

hold off;
plot(eps(:,3),eps(:,6)/201,'-','color',[0.6 0 0],'Linewidth',2);
hold on;
plot(nu(1:51,2),nu(1:51,6)/201,':','color',[0 0.4 0],'Linewidth',3);
plot(mu(51:151,1)-10,mu(51:151,6)/201,'--','color',[0 0 0.8],'Linewidth',2);

xlim([-5 5]);
ylim([-0.2 0]);
xlabel('\mu, \nu, \epsilon (Wasserstein scaling)');
ylabel('Ellipsoidal Energy');
set(gca,'FontSize',20);
set(gca,'xtick',[-4 -2 0 2 4]);
set(gca,'ytick',[-0.2 -0.1 0]);
