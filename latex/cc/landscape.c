/* landscape.c
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MAX_DIM        100
#define NUM_POINTS    1000

#define  MAX_INT_PLUS_ONE  2147483648.0

double random_gaussian();

void initialize( double a[NUM_POINTS][MAX_DIM] );

void rescale(
	     double a[NUM_POINTS][MAX_DIM],
	     double b[NUM_POINTS][MAX_DIM],
	     double scale[NUM_POINTS],
	     double sigma[MAX_DIM]
	     );

double compute_wass1(
		     double a[NUM_POINTS][MAX_DIM],
		     double b[NUM_POINTS][MAX_DIM]
		    );

double compute_wass2(
		     double a[NUM_POINTS][MAX_DIM],
		     double b[NUM_POINTS][MAX_DIM]
		    );

double compute_loss(double b[NUM_POINTS][MAX_DIM]);

int main( void )
{
  double a[NUM_POINTS][MAX_DIM];
  double b[NUM_POINTS][MAX_DIM];
  double eta[NUM_POINTS];
  double scale[NUM_POINTS];
  double sigma[MAX_DIM];
  double mu,nu,eps;
  double wass1,wass2;
  double loss;
  double sumsq;
  int j,n;

  initialize(a);

  for( j=0; j < MAX_DIM; j++ ) {
    sigma[j] = 1.0;
  }
  for( n=0; n < NUM_POINTS; n++ ) {
    eta[n] = random_gaussian();
  }

  eps = 0.0;
  mu = sqrt((double)MAX_DIM);
  nu = 0.0;

  //for( mu = sqrt((double)MAX_DIM)-1.0; mu <= ((double)MAX_DIM)+1.0; mu = mu + 0.1 ) {
  //for( nu = 0.0; nu <= 2.0; nu = nu + 0.1 ) {
  for( eps = 0.0; eps <= sqrt((double)MAX_DIM)+0.001; eps = eps + 0.1 ) {
    sumsq = 0.0;
    //for( j=0; j < MAX_DIM; j++ ) {
    //  sigma[j] = sqrt(1.0 - 2*eps*sqrt(3.0/(MAX_DIM))
    //	          + 4*eps*sqrt(3.0/(MAX_DIM))*j/(MAX_DIM-1));
    //  sumsq += sigma[j]*sigma[j];
    //}
    for( j=0; j < MAX_DIM/2; j++ ) {
      sigma[j] = sqrt(1.0 - 2.0*eps*sqrt(1.0/MAX_DIM));
    }
    for( j = MAX_DIM/2; j < MAX_DIM; j++ ) {
      sigma[j] = sqrt(1.0 + 2.0*eps*sqrt(1.0/MAX_DIM));
    }
    //printf("sumsq = %1.4f\n",sumsq);
    for( n=0; n < NUM_POINTS; n++ ) {
      scale[n] = mu + nu * eta[n];
    }
    rescale(a,b,scale,sigma);
    wass1 = compute_wass1(a,b);
    wass2 = compute_wass2(a,b);
    loss = compute_loss(b)/(NUM_POINTS*(NUM_POINTS-1));
    printf("%1.1f %1.1f %1.1f %1.4f %1.4f %1.8f\n",mu,nu,eps,wass1,wass2,loss);//(2*MAX_DIM+1));
  }

  return 0;
}

void rescale(
	     double a[NUM_POINTS][MAX_DIM],
	     double b[NUM_POINTS][MAX_DIM],
	     double scale[NUM_POINTS],
	     double sigma[MAX_DIM]
	    )
{
  double old_norm, new_norm;
  int n,j;

  for( n=0; n < NUM_POINTS; n++ ) {
    old_norm = 0.0;
    for( j=0; j < MAX_DIM; j++ ) {
      old_norm += a[n][j] * a[n][j];
    }
    old_norm = sqrt(old_norm);
    for( j=0; j < MAX_DIM; j++ ) {
      b[n][j] = sigma[j] * a[n][j] * scale[n]/old_norm;
    }
  }
}

double compute_wass2(
		     double a[NUM_POINTS][MAX_DIM],
		     double b[NUM_POINTS][MAX_DIM]
		    )
{
  double wass2;
  int j,n;

  wass2 = 0.0;
  for( n=0; n < NUM_POINTS; n++ ) {
    for( j=0; j < MAX_DIM; j++ ) {
      wass2 += (a[n][j] - b[n][j])*(a[n][j] - b[n][j]);
    }
  }

  return(sqrt(wass2/NUM_POINTS));
}

double compute_wass1(
		     double a[NUM_POINTS][MAX_DIM],
		     double b[NUM_POINTS][MAX_DIM]
		    )
{
  double wass1,norm;
  int j,n;

  wass1 = 0.0;
  for( n=0; n < NUM_POINTS; n++ ) {
    norm = 0.0;
    for( j=0; j < MAX_DIM; j++ ) {
      norm += (a[n][j] - b[n][j])*(a[n][j] - b[n][j]);
    }
    wass1 += sqrt(norm);
  }

  return(wass1/NUM_POINTS);
}

double compute_loss(double b[NUM_POINTS][MAX_DIM])
{
  double norm[NUM_POINTS] = { 0.0 };
  double norm_mn;
  double loss;
  int i,m,n;

  for( n=0; n < NUM_POINTS; n++ ) {
    for( i=0; i < MAX_DIM; i++ ) {
      norm[n] += b[n][i] * b[n][i];
    }
  }

  loss = 0.0;
  for( m=0; m < NUM_POINTS; m++ ) {
    for( n=0; n < NUM_POINTS; n++ ) {
      norm_mn = 0.0;
      for( i=0; i < MAX_DIM; i++ ) {
	norm_mn +=(b[m][i]-b[n][i])*(b[m][i]-b[n][i]);
      }
      loss += 0.5*(norm[m]+norm[n])
	- (2*MAX_DIM+1)*log(1.0+norm_mn/(2*MAX_DIM+1));
    }
  }

  return loss;
}

void initialize( double a[NUM_POINTS][MAX_DIM] )
{
  double norm;
  int i,n;

  for( n=0; n < NUM_POINTS; n++ ) {
    norm = 0.0;
    for( i=0; i < MAX_DIM; i++ ) {
      //a[n][i] = 1.0 + 0.1*random_gaussian();
      //a[n][i] = 1.0 + 0.5*random_gaussian();
      a[n][i] = random_gaussian();
      norm += a[n][i]*a[n][i];
    }
    for( i=0; i < MAX_DIM; i++ ) {
      a[n][i] = a[n][i]*sqrt(MAX_DIM/norm);
    }
  }
}

double random_gaussian()
{
  static double V1, V2, S;
  static int random_gaussian_phase = 0;
  double X;
  if(random_gaussian_phase == 0) {
    do {
      double U1 = (double) random() / MAX_INT_PLUS_ONE;
      double U2 = (double) random() / MAX_INT_PLUS_ONE;
      V1 = 2.0 * U1 - 1.0;
      V2 = 2.0 * U2 - 1.0;
      S = V1 * V1 + V2 * V2;
    } while( S >= 1.0 || S == 0.0 );

    X = V1 * sqrt(-2.0 * log(S) / S);
  }
  else {
    X = V2 * sqrt(-2.0 * log(S) / S);
  }
  random_gaussian_phase = 1 - random_gaussian_phase;

  return X;
}
