CS = [
    0.1  137.0  58.1
    0.2   84.0  58.9
    0.3   66.3  58.8
    0.4   64.5  61.3
    0.5   67.3  62.0
    0.6   68.5  64.8
];

FS = [
    0.1  151.0  60.4
    0.2  103.9  59.6
    0.3   78.3  60.5
    0.4   68.1  62.7
    0.5   64.7  62.0
    0.6   65.8  63.8
];

hold off;
plot(CS(:,1),CS(:,2),"b--","linewidth",2);
hold on;
plot(CS(:,1),CS(:,3),"b","linewidth",2);
%plot(FS(:,1),FS(:,2),"k:","linewidth",2);
%plot(FS(:,1),FS(:,3),"k-.","linewidth",2);

xlabel('weighting parameter \lambda')
ylabel('Frechet Inception Score')

line([0.1 0.12],[63 63],'LineWidth',2,'Color','Black');
tvae = text(0.13,63,'VAE');
set(tvae,'FontSize',20);

line([0.1 0.12],[55 55],'LineWidth',2,'Color','Black');
twae = text(0.13,55,'WAE-MMD');
set(twae,'FontSize',20);

line([0.1 0.12],[42 42],'LineWidth',2,'Color','Black');
twgan = text(0.13,42,'WAE-GAN');
set(twgan,'FontSize',20);

tmatch = text(0.4,55,'PPAE (matched)');
set(tmatch,'FontSize',20);

tstand = text(0.4,73,'PPAE (standard)');
set(tstand,'FontSize',20);

xlim([0.1 0.6])
ylim([20 120])

set(gca,'xtick',[0.1 0.2 0.3 0.4 0.5 0.6])
set(gca,'ytick',[20 40 60 80 100 120])
set(gca,'FontSize',20)

%lgd = legend(' fine,     normal','coarse, normal',' fine,     skew','coarse, skew');
%set(lgd,'FontSize',20);

