"""
Copyright (C) 2018 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-SA 4.0 license (https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).
"""
from torch import nn
from torch.autograd import Variable
import torch
import torch.nn.functional as F
import random
import pdb
try:
    from itertools import izip as zip
except ImportError: # will be 3.x series
    pass

def mycrop(raw,crop):
    if crop == 0:
        cropped = raw
    else:
        size_x = raw.shape[2] - crop
        size_y = raw.shape[3] - crop
        cropped = torch.cuda.FloatTensor(raw.shape[0],raw.shape[1],size_x,size_y)
        for i in range(0,raw.shape[0]):
            dx = random.randint(0,crop)
            dy = random.randint(0,crop)
            cropped[i,:,:,:] = raw[i,:,dx:size_x+dx,dy:size_y+dy]
    return cropped

# Imagenet architecture
class BiDis(nn.Module):
    # Multi-scale discriminator architecture
    def __init__(self, input_dim, params):
        super(BiDis, self).__init__()

        nc = params['nc']
        crop = params['crop']
        hid_x = params['hid_x']
        hid_z = params['hid_z']
        hid_dis = params['hid_dis']

        self.dx = nn.Sequential(
            # input is  64 x 64
            nn.Conv2d(nc, 64, 4, 2, 0, bias=False),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),
            # state size.  31 x 31
            nn.Conv2d(64, 64, 4, 1, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),

            # state size. 28 x 28
            nn.Conv2d(64, 128, 4, 2, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),

            # state size. 13 x 13
            nn.Conv2d(128, 128, 4 - int(crop/2), 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),

            # state size. 10 x 10
            nn.Conv2d(128, 256, 4, 2, 0, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),

            # state size. 4 x 4
            nn.Conv2d(256, hid_x, 4, 1, 0, bias=False),
            nn.BatchNorm2d(hid_x),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False)
        )

        self.dz = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(256, 2048, 1, 1, 0, bias=False),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),

            # state size. (ndf) x 32 x 32
            nn.Conv2d(2048, hid_z, 1, 1, 0, bias=False),
            nn.LeakyReLU(0.1, inplace=True),
            # state size. (ndf*2) x 16 x 16
            torch.nn.Dropout2d(p=0.2, inplace=False)

        )
        self.dxz = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(hid_x+hid_z, hid_dis, 1, 1, 0, bias=False),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),

            # state size. (ndf) x 32 x 32
            nn.Conv2d(hid_dis, hid_dis, 1, 1, 0, bias=False),
            nn.LeakyReLU(0.1, inplace=True),
            torch.nn.Dropout2d(p=0.2, inplace=False),
            nn.Conv2d(hid_dis, 4, 1, 1, 0, bias=False),
            # torch.nn.Dropout2d(p=0.2, inplace=False),
            nn.LogSoftmax()
        )

    def forward(self, x, z):
        #if input.is_cuda and self.ngpu > 1:
        #    output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        #else:
        dx = self.dx(x)
        dz = self.dz(z)
        dxz = self.dxz(torch.cat([dx,dz],dim=1))
        return dxz.squeeze(1)



# ##################################################################################
# # Generator
# ##################################################################################

class BiGen(nn.Module):
    # AdaIN auto-encoder architecture
    def __init__(self, input_dim, params):
        super(BiGen, self).__init__()
        crop = params['crop']
        hid_gen1 = params['hid_gen1']
        hid_gen2 = params['hid_gen2']

        self.main = nn.Sequential(
            # 1x1
            nn.Conv2d(256, hid_gen1, 1, 1, 0, bias=False),
            nn.BatchNorm2d(hid_gen1),
            nn.LeakyReLU(0.1, inplace=True),

            # 1 x 1
            nn.Conv2d(hid_gen1, hid_gen2, 1, 1, 0, bias=False),
            nn.BatchNorm2d(hid_gen2),
            nn.LeakyReLU(0.1, inplace=True),

            # 1 x 1
            nn.ConvTranspose2d( hid_gen2, 256, 4, 1, 0, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=True),

            # 4 x 4
            nn.ConvTranspose2d(256, 128, 4, 2, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),

            # 10 x 10
            nn.ConvTranspose2d(128, 128,  4 - int(crop/2), 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),

            # 12 x 12
            nn.ConvTranspose2d(128, 64, 4, 2, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1, inplace=True),

            nn.ConvTranspose2d(64, 64, 4, 1, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1,inplace=True),
            nn.ConvTranspose2d(64, 64, 4, 2, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1,inplace=True),
            nn.ConvTranspose2d(64, 3, 1, 1, 0, bias=False),
            nn.Tanh()
            # state size. (nc) x 64 x 64
        )

    def forward(self, input):
        # if input.is_cuda and self.ngpu > 1:
        #     output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        # else:
        output = self.main(input)
        return output


class BiEn(nn.Module):
    # AdaIN auto-encoder architecture
    def __init__(self, input_dim, params):
        super(BiEn, self).__init__()
        crop = params['crop']
        hid_enc = params['hid_enc']

        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(3, 64, 4, 2, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(64, 64, 4, 1, 0, bias=False),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(64, 128, 4, 2, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(128, 128, 4 - int(crop/2), 1, 0, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(128, 256, 4, 2, 0, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(256, hid_enc, 4, 1, 0, bias=False),
            nn.BatchNorm2d(hid_enc),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(hid_enc, 1024, 1, 1, 0, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(1024, 1024, 1, 1, 0, bias=False),
            nn.BatchNorm2d(1024),
            nn.LeakyReLU(0.1, inplace=True),

            nn.Conv2d(1024, 256, 1, 1, 0, bias=False),

        )

    def forward(self, input):

        output = self.main(input)
        return output



# Cifar10 architecture
# class BiDis(nn.Module):
#     # Multi-scale discriminator architecture
#     def __init__(self, input_dim, params):
#         super(BiDis, self).__init__()
#
#         nc = params['nc']
#         crop = params['crop']
#
#         self.dx = nn.Sequential(
#             # input is (nc) x 32 x 32
#             nn.Conv2d(nc, 32, 5, 1, 0, bias=False),
#             nn.LeakyReLU(0.1, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#
#             # state size. (ndf) x 28 x 28
#             nn.Conv2d(32, 64, 4, 2, 0, bias=False),
#             nn.BatchNorm2d(64),
#             nn.LeakyReLU(0.1, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#
#             # state size. (ndf*2) x 13 x 13
#             nn.Conv2d(64, 128, 4-crop, 1, 0, bias=False),
#             nn.BatchNorm2d(128),
#             nn.LeakyReLU(0.1, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#
#             # state size. (ndf*2) x 10 x 10
#             nn.Conv2d(128, 256, 4, 2, 0, bias=False),
#             nn.BatchNorm2d(256),
#             nn.LeakyReLU(0.1, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#
#             # state size. (ndf*4) x 4 x 4
#             nn.Conv2d(256, 512, 4, 1, 0, bias=False),
#             nn.BatchNorm2d(512),
#             nn.LeakyReLU(0.1, inplace=True),
#
#         )
#
#         self.dz = nn.Sequential(
#             # input is (nc) x 64 x 64
#             nn.Conv2d(64, 512, 1, 1, 0, bias=False),
#             nn.LeakyReLU(0.1, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#
#             # state size. (ndf) x 32 x 32
#             nn.Conv2d(512, 512, 1, 1, 0, bias=False),
#             nn.LeakyReLU(0.1, inplace=True),
#             # state size. (ndf*2) x 16 x 16
#             torch.nn.Dropout2d(p=0.2, inplace=False)
#
#         )
#         self.dxz = nn.Sequential(
#             # input is (nc) x 64 x 64
#             nn.Conv2d(1024, 1024, 1, 1, 0, bias=False),
#             nn.LeakyReLU(0.1, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#
#             # state size. (ndf) x 32 x 32
#             nn.Conv2d(1024, 1024, 1, 1, 0, bias=False),
#             nn.LeakyReLU(0.2, inplace=True),
#             torch.nn.Dropout2d(p=0.2, inplace=False),
#             nn.Conv2d(1024, 4, 1, 1, 0, bias=False),
#             # torch.nn.Dropout2d(p=0.2, inplace=False),
#             nn.LogSoftmax()
#             # state size. (ndf*2) x 16 x 16
#
#         )
#
#     def forward(self, x, z):
#         #if input.is_cuda and self.ngpu > 1:
#         #    output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
#         #else:
#         dx = self.dx(x)
#         dz = self.dz(z)
#         dxz = self.dxz(torch.cat([dx,dz],dim=1))
#         return dxz.squeeze(1)
#
#
#
# ##################################################################################
# # Generator
# ##################################################################################
#
# class BiGen(nn.Module):
#     # AdaIN auto-encoder architecture
#     def __init__(self, input_dim, params):
#         super(BiGen, self).__init__()
#
#         nz = params['nz']
#         nc = params['nc']
#         crop = params['crop']
#         print(nz)
#         self.main = nn.Sequential(
#
#             nn.ConvTranspose2d(nz, 256, 4, 1, 0, bias=False),
#             nn.BatchNorm2d(256),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             nn.ConvTranspose2d(256, 128, 4, 2, 0, bias=False),
#             nn.BatchNorm2d(128),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # input is Z, going into a convolution
#             nn.ConvTranspose2d(128, 64, 4-crop, 1, 0, bias=False),
#             nn.BatchNorm2d(64),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             nn.ConvTranspose2d(64, 32, 4, 2, 0, bias=False),
#             nn.BatchNorm2d(32),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             nn.ConvTranspose2d(32, 32, 5, 1, 0, bias=False),
#             nn.BatchNorm2d(32),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             nn.Conv2d(32, 32, 1, 1, 0, bias=False),
#             nn.BatchNorm2d(32),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             nn.Conv2d(32, 3, 1, 1, 0, bias=False),
#
#             # TODO: tanh or sigmoid?
#             nn.Tanh()
#         )
#
#     def forward(self, input):
#         # if input.is_cuda and self.ngpu > 1:
#         #     output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
#         # else:
#         output = self.main(input)
#         return output
#
#
# class BiEn(nn.Module):
#     # AdaIN auto-encoder architecture
#     def __init__(self, input_dim, params):
#         super(BiEn, self).__init__()
#         nz = params['nz']
#
#         nc = params['nc']
#         crop = params['crop']
#
#         self.main = nn.Sequential(
#             # input is (nc) x 32 x 32
#             nn.Conv2d(nc, 32, 5, 1, 0, bias=False),
#             nn.BatchNorm2d(32),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # state size. (ndf) x 28 x 28
#             nn.Conv2d(32, 64, 4, 2, 0, bias=False),
#             nn.BatchNorm2d(64),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # state size. (ndf*2) x 13 x 13
#             nn.Conv2d(64, 128, 4-crop, 1, 0, bias=False),
#             nn.BatchNorm2d(128),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # state size. (ndf*2) x 10 x 10
#             nn.Conv2d(128, 256, 4, 2, 0, bias=False),
#             nn.BatchNorm2d(256),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # state size. (ndf*4) x 4 x 4
#             nn.Conv2d(256, 512, 4, 1, 0, bias=False),
#             nn.BatchNorm2d(512),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # state size. (ndf*8) x 1 x 1
#             nn.Conv2d(512, 512, 1, 1, 0, bias=False),
#             nn.BatchNorm2d(512),
#             nn.LeakyReLU(0.1, inplace=True),
#
#             # state size. (ndf*8) x 1 x 1
#             nn.Conv2d(512, 64, 1, 1, 0, bias=False)
#         )
#
#     def forward(self, input):
#         # if input.is_cuda and self.ngpu > 1:
#         #     output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
#         # else:
#         output = self.main(input)
#         return output
