from torch.utils import data
from torchvision import transforms as T
from torchvision.datasets import ImageFolder
import torchvision.datasets as dset
import torchvision.transforms as transforms
from PIL import Image
import torch
import os
import random,pickle
import numpy as np
import pdb
from collections import defaultdict
from PIL import Image

def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo)
    return dict



class CelebA(data.Dataset):
    """Dataset class for the CelebA dataset."""

    def __init__(self, image_dir, attr_path, selected_num, transform, mode):
        """Initialize and preprocess the CelebA dataset."""
        self.image_dir = image_dir
        self.attr_path = attr_path
        self.selected_num = selected_num
        self.transform = transform
        self.mode = mode
        self.train_dataset = []
        self.test_dataset = []

        self.preprocess()

        if mode == 'train':
            self.num_images = len(self.train_dataset)
        else:
            self.num_images = len(self.test_dataset)

    def preprocess(self):
        """Preprocess the CelebA attribute file."""
        lines = [line.rstrip() for line in open(self.attr_path, 'r')]

        self.dic_content = defaultdict(list)
        self.dic_count = defaultdict(int)
        self.dic_identity = {}

        # lines = lines[2:]
        random.seed(1234)
        random.shuffle(lines)

        for i, line in enumerate(lines):
            split = line.split()
            filename = split[0]
            label = split[1]

            self.dic_content[label].append(filename)
            self.dic_count[label] += 1
            self.dic_identity[filename] = label


        rank = sorted(list(self.dic_count.items()), key=lambda x: x[1], reverse=True)

        self.selected = [a[0] for a in rank[:self.selected_num]]

        for label, name in enumerate(self.selected):
            for filename in self.dic_content[name]:
                self.train_dataset.append([filename, label])

            # if (i+1) < 2000:
            #     self.test_dataset.append([filename, label])
            # else:
            #     self.train_dataset.append([filename, label])

        print('Finished preprocessing the CelebA dataset...')

    def __getitem__(self, index):
        """Return one image and its corresponding attribute label."""
        dataset = self.train_dataset if self.mode == 'train' else self.test_dataset

        # I1A1
        filename, label = dataset[index]

        # Get I1A2
        identity = self.dic_identity[filename]
        files = self.dic_content[identity]
        filename2 = random.choice(files)

        # Get I2A2

        label2 = random.randint(0,self.selected_num-1)

        while label2 == label:
            label2 = random.randint(0,self.selected_num-1)

        identity2 = self.selected[label2]
        files2 = self.dic_content[identity2]
        filename3 = random.choice(files2)

        image = Image.open(os.path.join(self.image_dir, filename))
        image2 = Image.open(os.path.join(self.image_dir, filename2))
        image3 = Image.open(os.path.join(self.image_dir, filename3))

        # TODO: Label datatype?
        return self.transform(image), label, self.transform(image2), label, self.transform(image3), label2

    #torch.FloatTensor([label])

    def __len__(self):
        """Return the number of images."""
        return self.num_images


class SinglePickleDataset(data.Dataset):

    def __init__(self, image_dir, transform):
        """Initialize and preprocess the CelebA dataset."""
        self.image_dir = image_dir
        self.transform = transform

        self.data_file = image_dir

        data = unpickle(self.data_file)
        x = data['data']
        self.y = data['labels']

        img_size = 64
        img_size2 = img_size * img_size

        x = np.dstack((x[:, :img_size2], x[:, img_size2:2 * img_size2], x[:, 2 * img_size2:]))
        self.x = x.reshape((x.shape[0], img_size, img_size, 3))

        # self.x = x.transpose(0, 3, 1, 2)

    def __getitem__(self, index):
        """Return one image and its corresponding attribute label."""
        x = self.x[index]
        y = self.y[index]


        if self.transform:
            x = self.transform(x)


        return x,y


    def __len__(self):
        """Return the number of images."""
        return len(self.x)

def image64_loader(image_dir,batch_size,num_workers):
    list_of_datasets = []




    normalize = transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    )
    transform = transforms.Compose([transforms.ToPILImage(),
                                       transforms.RandomHorizontalFlip(),
                                       transforms.ToTensor(),
                                       normalize,
                                   ])
    for i in range(1,10):
        path = os.path.join(image_dir, 'train_data_batch_%d' % i)
        list_of_datasets.append(SinglePickleDataset(path, transform=transform))

    multiple_dataset = data.ConcatDataset(list_of_datasets)





    return multiple_dataset

def get_loader(image_dir, crop_size=128, image_size=64,
               batch_size=64, dataset='cifar10', mode='train', num_workers=1):
    """Build and return a data loader."""
    # if mode == 'train':
    #     transform.append(T.RandomHorizontalFlip())
    # transform.append(T.CenterCrop(crop_size))
    # transform.append(T.Resize(image_size))
    # transform.append(T.ToTensor())
    # transform.append(T.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5)))
    # transform = T.Compose(transform)



    if dataset == 'imagenet':
        # folder dataset
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )

        dataset = dset.ImageFolder(root=image_dir,
                                   transform=transforms.Compose([
                                       transforms.Resize((64,64)),
                                       transforms.RandomHorizontalFlip(),
                                       transforms.ToTensor(),
                                       normalize,
                                   ]))


    elif dataset == 'imagenet64':

        dataset = image64_loader(image_dir, batch_size,num_workers)

    elif dataset == 'cifar10':
        dataset = dset.CIFAR10(root=image_dir, download=True,
                               transform=transforms.Compose([
                                   #transforms.Resize(image_size),
                                   transforms.RandomHorizontalFlip(),
                                   #transforms.RandomCrop(image_size),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                               ]))

    elif dataset == 'mnist':

        dataset = dset.MNIST(root=image_dir, download=True,
                             transform=transforms.Compose([
                                 transforms.ToTensor()
                             ]))





    data_loader = data.DataLoader(dataset=dataset,
                                  batch_size=batch_size,
                                  shuffle=(mode=='train'),
                                  num_workers=num_workers)

    return data_loader
