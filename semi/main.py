import argparse
import torch
from torchvision import datasets, transforms
import torch.optim as optim
from torch.autograd import Variable
import torchvision.utils as vutils
from semi.model import *
import os,pdb,math
from torch import autograd
from semi.utils import weights_init, log_sum_exp, get_log_odds, puddingMat, tocuda,boolean_string, puddingScaled

batch_size = 100
lr = 1e-4
latent_size = 256
num_epochs = 100





parser = argparse.ArgumentParser()
parser.add_argument('--dataset', required=True, help='cifar10 | svhn')
parser.add_argument('--dataroot', required=True, help='path to dataset')
parser.add_argument('--use_cuda', action='store_true')
parser.add_argument('--save_model_dir', required=True)
parser.add_argument('--save_image_dir', required=True)
parser.add_argument('--sampling', action='store_true', help='enable sampling')
parser.add_argument('--pudding', action='store_true', help='enable plumb pudding loss')
parser.add_argument('--outputs', type=int,  default=3, help='number of softmax outputs')
parser.add_argument('--device', type=int,  default=0, help='cuda device')


opt = parser.parse_args()

f = open(opt.save_image_dir+"/args.txt", "w")
f.write(str(opt))
f.close()

cuda_device = opt.device

os.environ["CUDA_VISIBLE_DEVICES"] = str(cuda_device)
print(opt)



if opt.dataset == 'svhn':
    train_loader = torch.utils.data.DataLoader(
        datasets.SVHN(root=opt.dataroot, split='extra', download=False,
                      transform=transforms.Compose([
                          transforms.ToTensor()
                      ])),
        batch_size=batch_size, shuffle=True,num_workers=4)

elif opt.dataset == 'cifar10':
    train_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(root=opt.dataroot, train=True, download=True,
                      transform=transforms.Compose([
                          transforms.ToTensor()
                      ])),
        batch_size=batch_size, shuffle=True,num_workers=4)
else:
    raise NotImplementedError

netE = tocuda(Encoder(latent_size, opt.sampling))
netG = tocuda(Generator(latent_size))
netD = tocuda(Discriminator(latent_size, 0.2, opt.outputs))

netE.apply(weights_init)
netG.apply(weights_init)
netD.apply(weights_init)

optimizerG = optim.Adam([{'params' : netE.parameters()},
                         {'params' : netG.parameters()}], lr=lr, betas=(0.5,0.999))
optimizerD = optim.Adam(netD.parameters(), lr=lr, betas=(0.5, 0.999))

criterion = nn.BCELoss()

for epoch in range(num_epochs):

    i = 0
    for (data, target) in train_loader:
        # with autograd.detect_anomaly():
        real_label = Variable(tocuda(torch.ones(batch_size)))
        fake_label = Variable(tocuda(torch.zeros(batch_size)))

        if epoch == 0 and i == 0:
            netG.output_bias.data = get_log_odds(tocuda(data))

        if data.size()[0] != batch_size:
            continue

        d_real = Variable(tocuda(data))

        z_fake = Variable(tocuda(torch.randn(batch_size, latent_size, 1, 1)))
        d_fake = netG(z_fake)

        z_real, _, _, _ = netE(d_real)
        z_real = z_real.view(batch_size, -1)



        if opt.sampling:
            mu, log_sigma = z_real[:, :latent_size], z_real[:, latent_size:]
            sigma = torch.exp(log_sigma)
            epsilon = Variable(tocuda(torch.randn(batch_size, latent_size)))

            output_z = mu + epsilon * sigma
        else:

            output_z = z_real

        d_RxRz, _ = netD(d_real, output_z.view(batch_size, latent_size, 1, 1))
        d_FxFz, _ = netD(d_fake, z_fake)

        if opt.outputs != 2:
            d_FxRz,_ = netD(d_fake, output_z.view(batch_size, latent_size, 1, 1))



        loss_RxRz = -torch.mean(d_RxRz[:, 0])
        loss_FxFz = -torch.mean(d_FxFz[:, 1])

        if opt.outputs != 2:
            loss_FxRz = -torch.mean(d_FxRz[:, 2])


        if opt.outputs == 4:
            d_RxFz,_ = netD(d_real, z_fake)
            loss_RxFz = -torch.mean(d_RxFz[:, 3])
            loss_d = loss_RxRz + loss_FxFz + loss_FxRz + loss_RxFz

        elif opt.outputs ==3:
            loss_d = loss_RxRz + loss_FxFz + loss_FxRz

        elif opt.outputs == 2:
            loss_d = loss_RxRz + loss_FxFz


        if opt.pudding:
            loss_PP_real = puddingScaled(output_z)
            loss_g = -torch.mean(d_FxFz[:, 0] + d_RxRz[:, 1]) + loss_PP_real
        else:
            loss_g = -torch.mean(d_FxFz[:, 0] + d_RxRz[:, 1])

        if loss_g.item() < 3.5:
            optimizerD.zero_grad()
            loss_d.backward(retain_graph=True)
            optimizerD.step()
        optimizerG.zero_grad()
        loss_g.backward()
        optimizerG.step()


        if i % 1 == 0:
            print("Epoch :", epoch, "Iter :", i, "D Loss :", loss_d.item(), "G loss :", loss_g.item())
                #,
                #  "D(x) :", output_real.mean().item(), "D(G(x)) :", output_fake.mean().item())

        if i % 50 == 0:

            vutils.save_image(d_fake.cpu().data[:16, ], './%s/fake.png' % (opt.save_image_dir))
            vutils.save_image(d_real.cpu().data[:16, ], './%s/real.png'% (opt.save_image_dir))

            if opt.sampling:
                recon = netG(mu.view(batch_size, latent_size, 1, 1))
            else:
                recon = netG(output_z.view(batch_size, latent_size, 1, 1))

            vutils.save_image(recon.cpu().data[:16, ], './%s/recon.png'% (opt.save_image_dir))

        i += 1

    if epoch % 10 == 0:
        torch.save(netG.state_dict(), './%s/netG_epoch_%d.pth' % (opt.save_model_dir, epoch))
        torch.save(netE.state_dict(), './%s/netE_epoch_%d.pth' % (opt.save_model_dir, epoch))
        torch.save(netD.state_dict(), './%s/netD_epoch_%d.pth' % (opt.save_model_dir, epoch))

        if opt.sampling:
            recon = netG(mu.view(batch_size, latent_size, 1, 1))
        else:
            recon = netG(output_z.view(batch_size, latent_size, 1, 1))


        vutils.save_image(d_real.cpu().data[:16, ], './%s/real_%d.png' % (opt.save_image_dir, epoch))
        vutils.save_image(d_fake.cpu().data[:16, ], './%s/fake_%d.png' % (opt.save_image_dir, epoch))
        vutils.save_image(recon.cpu().data[:16, ], './%s/recon_%d.png' % (opt.save_image_dir, epoch))
