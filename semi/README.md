To train SVHN/CIFAR10:

`python3 main.py --dataset <dataset> --dataroot <dataroot> --use_cuda --save_model_dir <dir> --save_image_dir <dir>`


To test pretrained network for semi-supervised learning:

`python3 test_semisup.py --dataset=svhn --dataroot=<dataroot> --model_path=<saved_model_path>`

