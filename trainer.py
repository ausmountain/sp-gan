"""
Copyright (C) 2017 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-SA 4.0 license (https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).
"""
from networks_small import BiGen, BiEn, BiDis, mycrop
from utils import weights_initialization, get_model_list, vgg_preprocess, load_vgg16, get_scheduler
from torch.autograd import Variable
import torch.nn.functional as F
import torch
import torch.nn as nn
import random
import math
import os,pdb

class Trainer(nn.Module):
    def __init__(self, hyperparameters, resume_epoch, checkpoint_directory):
        super(Trainer, self).__init__()
        self.hyperparameters = hyperparameters
        lr = hyperparameters['lr']
        self.ngpu = hyperparameters['ngpu']
        # Initiate the networks
        self.gen = BiGen(hyperparameters['input_dim'], hyperparameters['gen'])  # Decoder
        self.enc = BiEn(hyperparameters['input_dim'], hyperparameters['enc'])   # Encoder
        self.dis = BiDis(hyperparameters['input_dim'], hyperparameters['dis'])  # discriminator


        print(self.dis)



        self.dis2 = None
        self.dis3 = None
        self.dis4 = None

        self.loss_D_avg = 10
        self.loss_D2_avg= 10
        self.loss_D3_avg= 10
        self.loss_D4_avg= 10
        print(self.gen)

        # Setup the optimizers
        beta1 = hyperparameters['beta1']
        beta2 = hyperparameters['beta2']
        epsilon = hyperparameters['epsilon']
        dis_params = list(self.dis.parameters())
        gen_params = list(self.gen.parameters())
        enc_params = list(self.enc.parameters())


        self.cuda()

        self.dis_opt = torch.optim.Adam([p for p in dis_params if p.requires_grad], eps=epsilon,
                                        lr=lr, betas=(beta1, beta2), weight_decay=hyperparameters['weight_decay'])
        self.gen_opt = torch.optim.Adam([p for p in gen_params if p.requires_grad], eps=epsilon,
                                        lr=lr, betas=(beta1, beta2), weight_decay=hyperparameters['weight_decay'])
        self.enc_opt = torch.optim.Adam([p for p in enc_params if p.requires_grad], eps=epsilon,
                                        lr=lr, betas=(beta1, beta2), weight_decay=hyperparameters['weight_decay'])

        if resume_epoch:
            self.resume(checkpoint_directory, resume_epoch)

        if self.ngpu > 1:
            # self = nn.DataParallel(self, list(range(self.ngpu)))
            self.gen = nn.DataParallel(self.gen, list(range(self.ngpu)))
            self.enc = nn.DataParallel(self.enc, list(range(self.ngpu)))
            self.dis = nn.DataParallel(self.dis, list(range(self.ngpu)))

        # Network weight initialization
        # self.apply(weights_init(hyperparameters['init']))
        # self.dis.apply(weights_init('gaussian'))
        # self.apply(weights_initialization)

    def gen_update(self, data, hyperparameters):
        self.gen.zero_grad()
        self.enc.zero_grad()

        crop = data.shape[2] - hyperparameters['image_size']
        if crop > 0 and hyperparameters['multicrop'] > 0:
            real_x_E = mycrop(data,crop)
            orig_x_E = mycrop(data,crop)
            real_z_E = self.enc(orig_x_E)
        else:
            real_x_E = data
            real_z_E = self.enc(real_x_E)

        batch_size = real_x_E.size(0)

        # generate samples for training Generator and Encoder
        fake_z_G = torch.randn(batch_size, hyperparameters['nz'], 1, 1).cuda()        
        #fake_z_G = torch.randn(batch_size, hyperparameters['nz'], 1, 1)
        fake_x_G = self.gen(fake_z_G)

        d_real_GE = self.dis(real_x_E, real_z_E)
        d_fake_GE = self.dis(fake_x_G, fake_z_G)

        self.loss_GE = -torch.mean(d_fake_GE[:,0] + d_real_GE[:,1])

        if self.dis2 != None:
            d2_real_GE = self.dis2(real_x_E, real_z_E)
            d2_fake_GE = self.dis2(fake_x_G, fake_z_G)
            self.loss2_GE = -torch.mean(d2_fake_GE[:,0] + d2_real_GE[:,1])
        else:
            self.loss2_GE = 0

        if self.dis3 != None:
            d3_real_GE = self.dis3(real_x_E, real_z_E)
            d3_fake_GE = self.dis3(fake_x_G, fake_z_G)
            self.loss3_GE = -torch.mean(d3_fake_GE[:,0] + d3_real_GE[:,1])
        else:
            self.loss3_GE = 0

        if self.dis4 != None:
            d4_real_GE = self.dis4(real_x_E, real_z_E)
            d4_fake_GE = self.dis4(fake_x_G, fake_z_G)
            self.loss4_GE = -torch.mean(d4_fake_GE[:,0] + d4_real_GE[:,1])
        else:
            self.loss4_GE = 0

        # apply L2 or PP loss to the G->E composition
        if hyperparameters['l2_f_w'] > 0 or hyperparameters['pp_f_w'] > 0:
            noise = torch.randn(batch_size, hyperparameters['nz'], 1, 1).cuda()
            fake_x = self.gen(noise)
            fake_z = self.enc(fake_x)

        if hyperparameters['l2_f_w'] > 0:
            self.loss_L2_fake = nn.functional.mse_loss(noise, fake_z)
        else:
            self.loss_L2_fake = 0

        # apply PP loss to the G->E composition
        if hyperparameters['pp_f_w'] > 0:
            #r_fake = torch.randperm(batch_size)
            #shuffle_fake = fake_z[r_fake,]
            #self.loss_PP_fake = puddingLoss(fake_z, shuffle_fake)
            self.loss_PP_fake = puddingMat(fake_z)
        else:
            self.loss_PP_fake = 0

        # apply L2 (reconstruction) loss to the E->G composition
        if hyperparameters['l2_r_w'] > 0:
            recon_x_E = self.gen(real_z_E)
            self.loss_L2_real = nn.functional.mse_loss(orig_x_E, recon_x_E)
        else:
            self.loss_L2_real = 0

        # apply PP cost to real images
        # when using 4 outputs, this PP loss must be included
        # because otherwise we get persistent mode collapse
        #r_real = torch.randperm(batch_size)
        #shuffle_real = real_z_E[r_real,]
        #self.loss_PP_real = puddingLoss(real_z_E, shuffle_real)
        #self.loss_PP_real = puddingMat(real_z_E)
        self.loss_PP_real = puddingScaled(real_z_E)

        # total loss
        self.loss_gen_total = hyperparameters['ge_w'] * self.loss_GE + \
                              hyperparameters['ge_w'] * self.loss2_GE + \
                              hyperparameters['ge_w'] * self.loss3_GE + \
                              hyperparameters['ge_w'] * self.loss4_GE + \
                              hyperparameters['pp_r_w'] * self.loss_PP_real + \
                              hyperparameters['pp_f_w'] * self.loss_PP_fake + \
                              hyperparameters['l2_r_w'] * self.loss_L2_real + \
                              hyperparameters['l2_f_w'] * self.loss_L2_fake

        self.loss_gen_total.backward()
        for p in list(self.gen.parameters()):
            p.grad = torch.where(torch.isfinite(p.grad), p.grad, torch.zeros_like(p.grad))
        self.gen_opt.step()
        for p in list(self.enc.parameters()):
            p.grad = torch.where(torch.isfinite(p.grad), p.grad, torch.zeros_like(p.grad))
        self.enc_opt.step()

    def sample(self, sample, noise):
        self.eval()

        fake = self.gen(noise).detach()

        recon = self.gen(self.enc(sample))

        self.train()
        return fake, recon

    def dis_update(self, data, hyperparameters):
        # generate data for training Discriminator
        crop = data.shape[2] - hyperparameters['image_size']
        if crop > 0 and hyperparameters['multicrop'] > 0:
            real_x_D = mycrop(data,crop)
            real_z_D = self.enc(mycrop(data,crop)).detach()
        else:
            real_x_D  = data
            real_z_D = self.enc(real_x_D).detach()

        batch_size = real_x_D.size(0)

        fake_z_D = torch.randn(batch_size, hyperparameters['nz'], 1, 1).cuda()
    
#        fake_z_D = torch.randn(batch_size, hyperparameters['nz'], 1, 1)
        fake_x_D = self.gen(fake_z_D).detach()

        # label = torch.full((batch_size,), real_label, device=device)

        self.dis_opt.zero_grad()
        d_RxRz = self.dis(real_x_D, real_z_D)
        d_FxFz = self.dis(fake_x_D, fake_z_D)

        self.loss_RxRz = -torch.mean(d_RxRz[:, 0])
        self.loss_FxFz = -torch.mean(d_FxFz[:, 1])

        if hyperparameters['nout'] == 2:
            self.loss_D = self.loss_RxRz + self.loss_FxFz
        else:
            # train Discriminator to distinguish all four combinations
            # RxRz, FxFz, RxFz, FxRz
            # this forces the Discriminator to focus on relationship
            # between x and z, rather than each component separately.
            # Generator is forced to produce image compatible with z,
            # Encoder is forced to produce z compatible with image.
            d_FxRz = self.dis(fake_x_D, real_z_D)
            self.loss_FxRz = -torch.mean(d_FxRz[:, 2])

            if hyperparameters['nout'] == 3:
                self.loss_D = self.loss_RxRz + self.loss_FxFz + self.loss_FxRz
            else:
                d_RxFz = self.dis(real_x_D, fake_z_D)
                self.loss_RxFz = -torch.mean(d_RxFz[:, 3])
                self.loss_D = self.loss_RxRz + self.loss_FxFz + self.loss_FxRz + self.loss_RxFz

        self.loss_D.backward()

        for p in list(self.dis.parameters()):
            p.grad = torch.where(torch.isfinite(p.grad), p.grad, torch.zeros_like(p.grad))
        self.dis_opt.step()

        self.loss_D_avg = 0.99*self.loss_D_avg + 0.01*self.loss_D.sum().item()

        if hyperparameters['nout'] < 3:
            loss_D_thresh = 0.3
        else:
            loss_D_thresh = 0.5

        num_discrim = hyperparameters['num_discrim']
        if self.dis2 == None and num_discrim > 1 and self.loss_D_avg < loss_D_thresh:
            self.dis2 = BiDis(hyperparameters['input_dim'], hyperparameters['dis'])
            lr = hyperparameters['lr']
            beta1 = hyperparameters['beta1']
            beta2 = hyperparameters['beta2']
            epsilon = hyperparameters['epsilon']
            dis2_params = list(self.dis2.parameters())
            self.dis2_opt = torch.optim.Adam([p for p in dis2_params if p.requires_grad], eps=epsilon,
                                lr=lr, betas=(beta1, beta2), weight_decay=hyperparameters['weight_decay'])
            #self.dis2.apply(weights_initialization)
            self.dis2.cuda()

            if self.ngpu > 1:
                self.dis2 = nn.DataParallel(self.dis2, list(range(self.ngpu)))


        if self.dis2 != None:
            self.dis2.zero_grad()

            d2_RxRz = self.dis2(real_x_D, real_z_D)
            d2_FxFz = self.dis2(fake_x_D, fake_z_D)

            self.loss2_RxRz = -torch.mean(d2_RxRz[:, 0])
            self.loss2_FxFz = -torch.mean(d2_FxFz[:, 1])

            if hyperparameters['nout'] == 2:
                self.loss_D2 = self.loss2_RxRz + self.loss2_FxFz
            else:
                d2_FxRz = self.dis2(fake_x_D, real_z_D)
                self.loss2_FxRz = -torch.mean(d2_FxRz[:, 2])

                if hyperparameters['nout'] == 3:
                    self.loss_D2 = self.loss2_RxRz + self.loss2_FxFz + self.loss2_FxRz
                else:
                    d2_RxFz = self.dis2(real_x_D, fake_z_D)
                    self.loss2_RxFz = -torch.mean(d2_RxFz[:, 3])
                    self.loss_D2 = self.loss2_RxRz + self.loss2_FxFz + self.loss2_FxRz + self.loss2_RxFz

            self.loss_D2.backward()

            for p in list(self.dis2.parameters()):
                p.grad = torch.where(torch.isfinite(p.grad), p.grad, torch.zeros_like(p.grad))
            self.dis2_opt.step()

            self.loss_D2_avg = 0.99*self.loss_D2_avg + 0.01*self.loss_D2.sum().item()

            if self.dis3 == None and num_discrim > 2 and self.loss_D2_avg < loss_D_thresh:
                self.dis3 = BiDis(hyperparameters['input_dim'], hyperparameters['dis'])
                lr = hyperparameters['lr']
                beta1 = hyperparameters['beta1']
                beta2 = hyperparameters['beta2']
                epsilon = hyperparameters['epsilon']
                dis3_params = list(self.dis3.parameters())
                self.dis3_opt = torch.optim.Adam([p for p in dis3_params if p.requires_grad], eps=epsilon,
                                lr=lr, betas=(beta1, beta2), weight_decay=hyperparameters['weight_decay'])
                #self.dis3.apply(weights_initialization)
                self.dis3.cuda()
                if self.ngpu > 1:
                    self.dis3 = nn.DataParallel(self.dis3, list(range(self.ngpu)))

        if self.dis3 != None:
            self.dis3.zero_grad()

            d3_RxRz = self.dis3(real_x_D, real_z_D)
            d3_FxFz = self.dis3(fake_x_D, fake_z_D)

            self.loss3_RxRz = -torch.mean(d3_RxRz[:, 0])
            self.loss3_FxFz = -torch.mean(d3_FxFz[:, 1])

            if hyperparameters['nout'] == 2:
                self.loss_D3 = self.loss3_RxRz + self.loss3_FxFz
            else:
                d3_FxRz = self.dis3(fake_x_D, real_z_D)
                self.loss3_FxRz = -torch.mean(d3_FxRz[:, 2])

                if hyperparameters['nout'] == 3:
                    self.loss_D3 = self.loss3_RxRz + self.loss3_FxFz + self.loss3_FxRz
                else:
                    d3_RxFz = self.dis3(real_x_D, fake_z_D)
                    self.loss3_RxFz = -torch.mean(d3_RxFz[:, 3])
                    self.loss_D3 = self.loss3_RxRz + self.loss3_FxFz + self.loss3_FxRz + self.loss3_RxFz

            self.loss_D3.backward()

            for p in list(self.dis3.parameters()):
                p.grad = torch.where(torch.isfinite(p.grad), p.grad, torch.zeros_like(p.grad))
            self.dis3_opt.step()

            self.loss_D3_avg = 0.99*self.loss_D3_avg + 0.01*self.loss_D3.sum().item()

            if self.dis4 == None and num_discrim > 3 and self.loss_D3_avg < loss_D_thresh:
                self.dis4 = BiDis(hyperparameters['input_dim'], hyperparameters['dis'])
                lr = hyperparameters['lr']
                beta1 = hyperparameters['beta1']
                beta2 = hyperparameters['beta2']
                epsilon = hyperparameters['epsilon']
                dis4_params = list(self.dis4.parameters())
                self.dis4_opt = torch.optim.Adam([p for p in dis4_params if p.requires_grad], eps=epsilon,
                                lr=lr, betas=(beta1, beta2), weight_decay=hyperparameters['weight_decay'])

                #self.dis4.apply(weights_initialization)
                self.dis4.cuda()
                if self.ngpu > 1:
                    self.dis4 = nn.DataParallel(self.dis4, list(range(self.ngpu)))

        if self.dis4 != None:
            self.dis4.zero_grad()

            d4_RxRz = self.dis4(real_x_D, real_z_D)
            d4_FxFz = self.dis4(fake_x_D, fake_z_D)

            self.loss4_RxRz = -torch.mean(d4_RxRz[:, 0])
            self.loss4_FxFz = -torch.mean(d4_FxFz[:, 1])

            if hyperparameters['nout'] == 2:
                self.loss_D4 = self.loss4_RxRz + self.loss4_FxFz
            else:
                d4_FxRz = self.dis4(fake_x_D, real_z_D)
                self.loss4_FxRz = -torch.mean(d4_FxRz[:, 2])

                if hyperparameters['nout'] == 3:
                    self.loss_D4 = self.loss4_RxRz + self.loss4_FxFz + self.loss4_FxRz
                else:
                    d4_RxFz = self.dis4(real_x_D, fake_z_D)
                    self.loss4_RxFz = -torch.mean(d4_RxFz[:, 3])
                    self.loss_D4 = self.loss4_RxRz + self.loss4_FxFz + self.loss4_FxRz + self.loss4_RxFz

            self.loss_D4.backward()

            for p in list(self.dis4.parameters()):
                p.grad = torch.where(torch.isfinite(p.grad), p.grad, torch.zeros_like(p.grad))
            self.dis4_opt.step()

            self.loss_D4_avg = 0.99*self.loss_D4_avg + 0.01*self.loss_D4.sum().item()

    def resume(self, checkpoint_dir, epoch):

        checkpoint_pth = '%s/checkpoint_epoch_%d.pth' % (checkpoint_dir, epoch)

        checkpoint = torch.load(checkpoint_pth)

        self.enc.load_state_dict(checkpoint['enc'])
        self.gen.load_state_dict(checkpoint['gen'])
        self.dis.load_state_dict(checkpoint['dis'])




        if checkpoint['dis2'] != None:
            self.dis2 = BiDis(self.hyperparameters['input_dim'], self.hyperparameters['dis'])

            lr = self.hyperparameters['lr']
            beta1 = self.hyperparameters['beta1']
            beta2 = self.hyperparameters['beta2']
            epsilon = self.hyperparameters['epsilon']
            dis2_params = list(self.dis2.parameters())
            self.dis2_opt = torch.optim.Adam([p for p in dis2_params if p.requires_grad], eps=epsilon,
                                             lr=lr, betas=(beta1, beta2), weight_decay=self.hyperparameters['weight_decay'])

            self.dis2.load_state_dict(checkpoint['dis2'])
            self.dis2_opt.load_state_dict(checkpoint['dis2_opt'])

            for state in self.dis2_opt.state.values():
                for k, v in state.items():
                    if isinstance(v, torch.Tensor):
                        state[k] = v.cuda()

            if self.ngpu > 1:
                self.dis2 = nn.DataParallel(self.dis2, list(range(self.ngpu)))

        if checkpoint['dis3'] != None:
            self.dis3 = BiDis(self.hyperparameters['input_dim'], self.hyperparameters['dis'])

            lr = self.hyperparameters['lr']
            beta1 = self.hyperparameters['beta1']
            beta2 = self.hyperparameters['beta2']
            epsilon = self.hyperparameters['epsilon']
            dis3_params = list(self.dis3.parameters())
            self.dis3_opt = torch.optim.Adam([p for p in dis3_params if p.requires_grad], eps=epsilon,
                                             lr=lr, betas=(beta1, beta2), weight_decay=self.hyperparameters['weight_decay'])

            self.dis3.load_state_dict(checkpoint['dis3'])
            self.dis3_opt.load_state_dict(checkpoint['dis3_opt'])

            for state in self.dis3_opt.state.values():
                for k, v in state.items():
                    if isinstance(v, torch.Tensor):
                        state[k] = v.cuda()
            if self.ngpu > 1:
                self.dis3 = nn.DataParallel(self.dis3, list(range(self.ngpu)))
        if checkpoint['dis4'] != None:
            self.dis4 = BiDis(self.hyperparameters['input_dim'], self.hyperparameters['dis'])

            lr = self.hyperparameters['lr']
            beta1 = self.hyperparameters['beta1']
            beta2 = self.hyperparameters['beta2']
            epsilon = self.hyperparameters['epsilon']
            dis4_params = list(self.dis4.parameters())
            self.dis4_opt = torch.optim.Adam([p for p in dis4_params if p.requires_grad], eps=epsilon,
                                             lr=lr, betas=(beta1, beta2), weight_decay=self.hyperparameters['weight_decay'])

            self.dis4.load_state_dict(checkpoint['dis4'])
            self.dis4_opt.load_state_dict(checkpoint['dis4_opt'])

            for state in self.dis4_opt.state.values():
                for k, v in state.items():
                    if isinstance(v, torch.Tensor):
                        state[k] = v.cuda()
            if self.ngpu > 1:
                self.dis4 = nn.DataParallel(self.dis4, list(range(self.ngpu)))
        self.enc_opt.load_state_dict(checkpoint['enc_opt'])
        self.gen_opt.load_state_dict(checkpoint['gen_opt'])
        self.dis_opt.load_state_dict(checkpoint['dis_opt'])

        for state in self.dis_opt.state.values():
            for k, v in state.items():
                if isinstance(v, torch.Tensor):
                    state[k] = v.cuda()


        self.loss_D_avg = checkpoint['loss_D_avg']
        self.loss_D2_avg= checkpoint['loss_D2_avg']
        self.loss_D3_avg= checkpoint['loss_D3_avg']
        self.loss_D4_avg= checkpoint['loss_D4_avg']


        print('Model loaded')
        print('Resume from epoch %d' % epoch)

        return epoch

    def save(self, snapshot_dir, epoch):

        checkpoint_pth = '%s/checkpoint_epoch_%d.pth' % (snapshot_dir, epoch)

        dis2_dict = None
        dis2_opt_dict = None
        dis3_dict = None
        dis3_opt_dict = None
        dis4_dict = None
        dis4_opt_dict = None

        if self.dis2 != None:
            dis2_dict = self.dis2.module.state_dict() if self.ngpu > 1 else self.dis2.state_dict()
            dis2_opt_dict = self.dis2_opt.state_dict()

        if self.dis3 != None:
            dis3_dict = self.dis3.module.state_dict() if self.ngpu > 1 else self.dis2.state_dict()
            dis3_opt_dict = self.dis3_opt.state_dict()
        if self.dis4 != None:
            dis4_dict = self.dis4.module.state_dict() if self.ngpu > 1 else self.dis4.state_dict()
            dis4_opt_dict = self.dis4_opt.state_dict()


        if self.ngpu > 1:


            torch.save(
                {'gen': self.gen.module.state_dict(), 'enc': self.enc.module.state_dict(),
                 'dis': self.dis.module.state_dict(),'dis2': dis2_dict, 'dis3': dis3_dict, 'dis4': dis4_dict,
                 'gen_opt': self.gen_opt.state_dict(), 'enc_opt': self.enc_opt.state_dict(),
                 'dis_opt': self.dis_opt.state_dict(),'dis2_opt': dis2_opt_dict, 'dis3_opt': dis3_opt_dict, 'dis4_opt': dis4_opt_dict,
                 'loss_D_avg': self.loss_D_avg, 'loss_D2_avg': self.loss_D2_avg, 'loss_D3_avg': self.loss_D3_avg,
                 'loss_D4_avg': self.loss_D4_avg},
                checkpoint_pth)

            print('saved multiple gpu model')
        else:
            torch.save(
                {'gen': self.gen.state_dict(),'enc': self.enc.state_dict(),'dis': self.dis.state_dict(),'dis2': dis2_dict, 'dis3': dis3_dict, 'dis4': dis4_dict,
                 'gen_opt': self.gen_opt.state_dict(), 'enc_opt': self.enc_opt.state_dict(),
                 'dis_opt': self.dis_opt.state_dict(), 'dis2_opt': dis2_opt_dict, 'dis3_opt': dis3_opt_dict, 'dis4_opt': dis4_opt_dict,
                 'loss_D_avg':self.loss_D_avg,'loss_D2_avg':self.loss_D2_avg,'loss_D3_avg':self.loss_D3_avg,'loss_D4_avg':self.loss_D4_avg},checkpoint_pth)

            print('saved single gpu model')



# custom loss function 1/2(z1^2 + z2^2) - Nlog(1+|z1-z2|^2)
def puddingLoss(z1, z2):
    batch_size = z1.size()[0]
    size = z1.size()[1]  # vector length

    reg = 0.5 * (torch.bmm(z1.view(batch_size, 1, size), z1.view(batch_size, size, 1))
               + torch.bmm(z2.view(batch_size, 1, size), z2.view(batch_size, size, 1)))
    diff = z1 - z2
    l2 = torch.bmm(diff.view(batch_size, 1, size), diff.view(batch_size, size, 1))

    result = reg/size - torch.log(1+l2)

    return math.sqrt(size)*torch.sum(result)/batch_size

def puddingMat(z):
    x0 = torch.squeeze(z)
    x1 = x0.transpose(0,1)

    batch_size = x0.size()[0]
    size = x0.size()[1]  # vector length

    xx  = torch.bmm(x0.view(batch_size, 1, size),
                    x0.view(batch_size, size, 1)).squeeze(2)
    xx0 = xx.expand(batch_size,batch_size)
    xx1 = xx0.transpose(0,1)

    xy = xx0 + xx1 - 2*torch.matmul(x0,x1)

    result = torch.sum(xx)/size - torch.sum(torch.log(1+xy))/batch_size

    return math.sqrt(size)*result/(batch_size-1)

def puddingScaled(z):
    x0 = torch.squeeze(z)
    x1 = x0.transpose(0,1)

    batch_size = x0.size()[0]
    size = x0.size()[1]  # vector length

    xx  = torch.bmm(x0.view(batch_size, 1, size),
                    x0.view(batch_size, size, 1)).squeeze(2)
    xx0 = xx.expand(batch_size,batch_size)
    xx1 = xx0.transpose(0,1)

    xy = xx0 + xx1 - 2*torch.matmul(x0,x1)

    result = torch.sum(xx)/size - 2*torch.sum(torch.log(1+xy/(2*size)))/batch_size

    return math.sqrt(size)*result/(batch_size-1)
