"""
Copyright (C) 2018 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-SA 4.0 license (https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).
"""
from utils import get_all_data_loaders, prepare_sub_folder, write_html, write_loss, get_config, write_2images, Timer
from networks import mycrop
import argparse
from torch.autograd import Variable
from trainer import Trainer
import torch.backends.cudnn as cudnn
import torch
from data_loader import get_loader,image64_loader

try:
    from itertools import izip as zip
except ImportError: # will be 3.x series
    pass
import os
import sys
import tensorboardX
import shutil,pdb
import torchvision.utils as vutils
from torchvision.utils import save_image

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default='cifar10', help='dataset.')
parser.add_argument('--config', type=str, default='configs/cifar10_folder.yaml', help='Path to the config file.')
parser.add_argument('--output_path', type=str, default='.', help="outputs path")
parser.add_argument('--resume_epoch', type=int, default=0, help='number of epochs to resume from')
opts = parser.parse_args()

cudnn.benchmark = True

# Load experiment setting
dataset = opts.dataset
config = get_config(opts.config)
max_iter = config['max_iter']
max_epoch = config['max_epoch']
display_size = config['display_size']

image_dir = config['data_root']


# Setup logger and output folders
model_name = os.path.splitext(os.path.basename(opts.config))[0]
train_writer = tensorboardX.SummaryWriter(os.path.join(opts.output_path + "/logs", model_name))
output_directory = os.path.join(opts.output_path + "/outputs", model_name)
checkpoint_directory, image_directory = prepare_sub_folder(output_directory)
shutil.copy(opts.config, os.path.join(output_directory, 'config.yaml')) # copy config file to output folder


# Setup model and data loader

trainer = Trainer(config, opts.resume_epoch, checkpoint_directory)


data_loader = get_loader(image_dir, config['crop_image_height'], config['image_size'], config['batch_size'],
                                   dataset, 'train', config['num_workers'])


data_iter = iter(data_loader)

fixed_data = next(data_iter)
fixed_data = fixed_data[0].cuda().detach()
crop = fixed_data.shape[2] - config['image_size']

if crop > 0 and config['multicrop'] > 0:
    fixed_data = mycrop(fixed_data,crop)

print(fixed_data.shape)


fixed_noise = torch.randn(config['batch_size'], config['nz'], 1, 1).cuda()




# Start training
# initial_epoch = trainer.resume(checkpoint_directory, opts.resume_epoch) if opts.resume_epoch else 0

trainer = trainer.cuda()

for epoch in range(opts.resume_epoch,max_epoch):
    for i, data in enumerate(data_loader, 0):


        data = data[0].cuda().detach()
        # data = data[0].detach()

        with Timer("Elapsed time in update: %f"):
        # Main training code
            trainer.dis_update(data, config)
            trainer.gen_update(data, config)

        torch.cuda.synchronize()

        # Dump training stats in log file
        # if (iterations + 1) % config['log_iter'] == 0:
        #     # print("Iteration: %08d/%08d" % (iterations + 1, max_iter))
        #     write_loss(iterations, trainer, train_writer)

        # iterations += 1
        if i % 10 == 0:
            if trainer.dis2 == None:
                print('[%d/%d][%d/%d] D: %.4f' % (epoch, max_epoch, i, len(data_loader), trainer.loss_D_avg))
            elif trainer.dis3 == None:
                print('[%d/%d][%d/%d] D: %.4f D2: %.4f' % (epoch, max_epoch, i, len(data_loader), trainer.loss_D_avg, trainer.loss_D2_avg))
            elif trainer.dis4 == None:
                print('[%d/%d][%d/%d] D: %.4f D2: %.4f D3: %.4f' % (epoch, max_epoch, i, len(data_loader), trainer.loss_D_avg, trainer.loss_D2_avg, trainer.loss_D3_avg))
            else:
                print('[%d/%d][%d/%d] D: %.4f D2: %.4f D3: %.4f D4: %.4f' % (epoch, max_epoch, i, len(data_loader), trainer.loss_D_avg, trainer.loss_D2_avg, trainer.loss_D3_avg, trainer.loss_D4_avg))
            sys.stdout.flush()

    # Save network weights
    if (epoch + 1) % config['snapshot_save_epoch'] == 0:
        trainer.save(checkpoint_directory, epoch + 1)
        print("save models")

    # Write images
    with torch.no_grad():
        fake, recon = trainer.sample(fixed_data, fixed_noise)

    vutils.save_image(fixed_data,'%s/real_samples.png' % image_directory,normalize=True)
    vutils.save_image(fake,'%s/fake_samples_epoch_%03d.png' % (image_directory, epoch),normalize=True)
    vutils.save_image(recon.detach(),'%s/recon_samples_epoch_%03d.png' % (image_directory, epoch),normalize=True)





    if epoch >= max_epoch:
        sys.exit('Finish training')
